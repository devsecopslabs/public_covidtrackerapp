const request = require('supertest');

const srv = require('../index');


test('Deve responder na pagina principal /', () => {
    return request(srv).get('/').then(res => {
        expect(res.status).toBe(200);
    });
});

test('Deve responder na pagina /countries', () => {
    return request(srv).get('/countries').then(res => {
        expect(res.status).toBe(200);
    });
});

test('Deve responder na pagina /continents', () => {
    return request(srv).get('/continents').then(res => {
        expect(res.status).toBe(200);
    });
});