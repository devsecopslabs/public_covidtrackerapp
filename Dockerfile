FROM node:lts-alpine AS buildapp
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install && npm audit fix --force
COPY . .

FROM node:lts-alpine3.13
EXPOSE 4000
WORKDIR /usr/src/app
COPY --from=buildapp /usr/src/app .
HEALTHCHECK CMD curl --fail http://localhost:4000/ || exit 1
CMD ["node", "server.js"]