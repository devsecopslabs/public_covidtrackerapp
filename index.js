const { response } = require('express');
const express = require('express');
const exhbs = require('express-handlebars');

//consutar como usar: https://www.npmjs.com/package/novelcovid
const api = require('novelcovid');

const app = express();

app.set('view engine', 'hbs');

app.engine( 'hbs', exhbs ({
    extname: 'hbs',
    defaultView: 'home',
    layoutsDir: __dirname + '/views/layouts'
}));

//pagina home para exibição dos dados de forma geral
app.get('/', (req, res) => {
    api.all().then((response) => {
        res.render('all', {info:response})
        //console.log(response);
    })
});


//pagina countries para exibição dos dados de cada pais
app.get('/countries', (req, res) => {

    api.countries()
    .then((response) =>  {
        res.render('home', {info:response});
    });
});

//pagina continentes
app.get('/continents', (req, res) => {
    api.continents()
    .then((response) => {
        res.render('continent', {info:response});
    });
});

module.exports = app;